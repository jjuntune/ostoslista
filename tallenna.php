<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Ostoslista</title>
  </head>
  <body>
    <div class="container">
      <h3>Ostoslista</h3>
      <div class="row">
        <div class="col">
        <?php
        $nimi = filter_input(INPUT_POST,'nimi',FILTER_SANITIZE_STRING);
        try {
          $tietokanta = new PDO('mysql:host=localhost;dbname=ostoslista;charset:utf-8','root','');

          $kysely = $tietokanta->prepare('insert into ostos (nimi, maara) values (:nimi,:maara)');
          $kysely->bindValue(':nimi',$nimi,PDO::PARAM_STR);
          $kysely->bindValue(':maara',1,PDO::PARAM_INT);
          $kysely->execute();
          print "<p>Ostos lisätty listaan!</p>";
        
        } catch (Exception $ex) {
            print "<p>Häiriö tietokantayhteydessä." . $ex->getMessage()  . "</p>";                        
        }
        ?>
        <a href="index.php">Etusivulle</a>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>