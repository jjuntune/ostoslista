create database ostoslista;

use ostoslista;

create table ostos (
  id int primary key auto_increment,
  nimi varchar(100) not null,
  maara smallint
);

